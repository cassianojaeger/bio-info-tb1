# Bruno Iochins Grisci - bigrisci@inf.ufrgs.br
# 04/05/2021
# INF05018 - Biologia Computacional
# Aulas 27 e 28: Análise de Dados de Expressão Gênica

import random
from collections import Counter

import matplotlib.pyplot as plt
import pandas as pd
from sklearn import metrics
from sklearn import svm
from sklearn.cluster import KMeans
from sklearn.metrics import confusion_matrix, f1_score
from sklearn.metrics import plot_confusion_matrix

file_name = 'Colorectal_GSE44076.csv'  # https://sbcb.inf.ufrgs.br/cumida


if __name__ == '__main__':
    data = pd.read_csv(file_name, delimiter=',', header=0, index_col=0)

    Y = data['type']
    X = data.drop('type', axis=1)
    total_columns = X.values.shape[1]

    #### NORMALIZE DATA BEGIN ####
    for col in range(total_columns):
        column_values = X.values[:, col]

        mean_value = column_values.mean()
        standard_deviation = column_values.std()

        for value in range(len(column_values)):
            column_values[value] = (column_values[value] -
                                    mean_value) / standard_deviation

    normalized_data = X
    normalized_data.insert(0, "type", Y, True)
    #### NORMALIZE DATA END ####

    #### SAMPLING DATA BEGIN ####
    set_of_types = Counter(Y)
    # print(set_of_types)

    number_of_total_samples = len(normalized_data.index)
    training_samples_count = round((number_of_total_samples * 70) / 100)
    testing_samples_count = number_of_total_samples - training_samples_count

    dataframe_list_by_type = []
    for types in set_of_types:
        dataframe_list_by_type.append((normalized_data[(normalized_data.type == types)]))

    dataframe_list_by_sample_purpose = {'training': [], 'testing': []}
    for types in dataframe_list_by_type:
        _number_of_total_samples = len(types.values)
        _training_samples_count = round((_number_of_total_samples * 70) / 100)
        _testing_samples_count = _number_of_total_samples - _training_samples_count

        chosen_indexes_training = random.sample(sorted(types.index.values), _training_samples_count)
        dataframe_list_by_sample_purpose.get('training').append(types.loc[chosen_indexes_training, :])
        types = types.drop(chosen_indexes_training)

        dataframe_list_by_sample_purpose.get('testing').append(types)

    training_frame = pd.concat(dataframe_list_by_sample_purpose.get('training'))
    testing_frame = pd.concat(dataframe_list_by_sample_purpose.get('testing'))

    Y_train = training_frame['type']
    X_train = training_frame.drop('type', axis=1)
    Y_test = testing_frame['type']
    X_test = testing_frame.drop('type', axis=1)
    #### SAMPLING DATA END ####

    #### CLASSIFIER CREATION - LINEAR KERNEL BEGIN ####
    clf = svm.SVC(kernel='linear')  # Linear Kernel
    clf.fit(X_train, Y_train)
    y_pred = clf.predict(X_test)

    print(y_pred)
    print(Y_test.values)
    #### CLASSIFIER CREATION - LINEAR KERNEL END ####

    #### CLASSIFIER PREDICTION METRICS BEGIN ####
    print("Accuracy:", metrics.accuracy_score(Y_test.values, y_pred))
    print("Precision:", metrics.precision_score(Y_test.values, y_pred,
                                                average="binary", pos_label="normal"))
    print("Sensitivity:", metrics.recall_score(Y_test.values, y_pred,
                                               average="binary", pos_label="normal"))
    plot_confusion_matrix(clf, X_test, Y_test)
    plt.show()

    Y_test = [1 if pred == 'normal' else 0 for pred in Y_test]
    y_pred = [1 if pred == 'normal' else 0 for pred in y_pred]

    tn, fp, fn, tp = confusion_matrix(Y_test, y_pred).ravel()
    specificity = tn / (tn + fp)
    print("Specificity:", specificity)
    print("F1-Score:", f1_score(Y_test, y_pred, zero_division=1))
    #### CLASSIFIER PREDICTION METRICS END ####

    #### K-MEANS CLUSTERS BEGIN ####
    kmeans = KMeans(n_clusters=4)
    kmeans_X = normalized_data.drop('type', axis=1)
    kmeans.fit(kmeans_X.values)
    print(kmeans.labels_)

    print(Counter(kmeans.labels_))
    #### K-MEANS CLUSTERS END ####