import sys


def print_d_matrix():
    global item
    print("\n")
    for item in D:
        print(item)


nodes = []


with open('test.txt') as f:
    taxa = next(f)
    D = [[float(x) for x in line.rstrip().split()] for line in f]

taxa = taxa.split()




print_d_matrix()


finished = False
count = 0
while not finished:
    count = count + 1
    u_list = []

    for line in D:
        u_list.append((sum(line)/(len(line)-2)))
    print("\nU List")
    print(u_list)

    chosen_row = 0
    chosen_col = 0
    least_val = sys.maxsize

    for row in range(0, len(D[0])):
        for col in range(0, len(D[0])):
            if row == col:
                continue
            calculated_val = D[row][col]-u_list[row]-u_list[col]
            if calculated_val < least_val:
                least_val = calculated_val
                chosen_row = row
                chosen_col = col

    print("\nchosen element: ")
    print(chosen_row, chosen_col, least_val)
    print(taxa)

    vrow = None
    vcol = None

    vi = (D[chosen_row][chosen_col]/2) + ((u_list[chosen_row] - u_list[chosen_col])/2)
    vj = (D[chosen_row][chosen_col]/2) + ((u_list[chosen_col] - u_list[chosen_row])/2)

    print("\nDistance Pairs")
    print(vi, vj)

    dxu = []

    for element in range(0, len(D[0])):
        if element == chosen_col or element == chosen_row:
            continue
        dxu.append((D[chosen_row][element] + D[element][chosen_col] - D[chosen_row][chosen_col])/2)

    print("\nDxU")
    print(dxu)

    [line.pop(chosen_row) for line in D]
    [line.pop(chosen_col-1) for line in D]

    D.pop(chosen_row)
    D.pop(chosen_col-1)
    print_d_matrix()

    for new_element in range(0, len(D)):
        D[new_element].insert(0, dxu[new_element])

    print_d_matrix()
    dxu.insert(0, 0)
    D.insert(0, dxu)
    print_d_matrix()

    new_element = "U"+str(count)

    nodes.append((new_element, (taxa[chosen_row], vi), (taxa[chosen_col], vj)))
    print(nodes)

    remove_element1 = taxa[chosen_row]
    remove_element2 = taxa[chosen_col]

    taxa.remove(remove_element1)
    taxa.remove(remove_element2)
    taxa.insert(0, new_element)
    print(taxa)

    if len(D) == 2:
        finished = True
        nodes.append((taxa[0], (taxa[1], D[0][1])))


print(nodes)
