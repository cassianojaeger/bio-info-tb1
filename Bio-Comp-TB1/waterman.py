match = 1
mismatch = -1
gap = -2

best_score = 0

def find_best_cel():
    max_cel_value = 0
    best_row, best_col = 0, 0
    for row in range(0, row_size):
        for col in range(0, col_size):
            if max_cel_value < pd_matrix[row][col]:
                max_cel_value = pd_matrix[row][col]
                best_row = row
                best_col = col

    return best_row, best_col, max_cel_value


def s(a, b):
    value = match if gen_sequences[1][a - 1] == gen_sequences[0][b - 1] else mismatch

    return value


def calc_cel(row, col):
    v1 = pd_matrix[row - 1][col - 1] + s(row, col)
    v2 = pd_matrix[row][col - 1] + gap
    v3 = pd_matrix[row - 1][col] + gap
    v4 = 0

    return v1, v2, v3, v4


def create_path_pair(row, col, op):
    pair = 0

    if op == "U":
        pair = (gen_sequences[1][row - 1], "-"), -4
    elif op == "L":
        pair = ("-", gen_sequences[0][col - 1]), -4
    elif op == "D":
        pair = (gen_sequences[1][row - 1], gen_sequences[0][col - 1]), s(row, col)

    return pair


def get_score():
    return pd_matrix[len(pd_matrix)-1][len(pd_matrix[0])-1]


gen_sequences = open("test2.txt").read().split()
# print(gen_sequences)

# AVTLI
row_size = len(gen_sequences[1]) + 1
# GVVYAH
col_size = len(gen_sequences[0]) + 1

pd_matrix = buckets = [[0 for col in range(col_size)] for row in range(row_size)]

# calculate matrix
for row in range(1, row_size):
    for col in range(1, col_size):
        pd_matrix[row][col] = max(calc_cel(row, col))

# Perform backtracking process
best_score_path = []
row, col, best_score = find_best_cel()

best_score_path.append(create_path_pair(row, col, "D"))

while True:  # Backtrack while true
    best_choice_map = {
        pd_matrix[row - 1][col]: (row - 1, col, "U"),  # UP
        pd_matrix[row][col - 1]: (row, col - 1, "L"),  # LEFT
        pd_matrix[row - 1][col - 1]: (row - 1, col - 1, "D")  # DIAGONAL
    }

    # Get Best choice (Between Up, Left or Diagonal movement)
    best_choice = best_choice_map.get((max(best_choice_map)))
    row, col, operation = best_choice[0], best_choice[1], best_choice[2]

    # If on first cell, finish backtrack
    if pd_matrix[row][col] == 0:
        break

    # create path pair and append to result list
    best_score_path.append(create_path_pair(row, col, operation))
# end while

seq1 = []
seq2 = []

print("Sequence Alignment")
for item in reversed(best_score_path):
    seq2.append(item[0][0])
    seq1.append(item[0][1])

print("".join(seq1))
print("".join(seq2))

print(f'Best score is {best_score}')

matches = list(filter(lambda path: path[1] == match, best_score_path))
identity = (len(matches)/len(best_score_path))*100
print(f'Identity score is {identity}%')


